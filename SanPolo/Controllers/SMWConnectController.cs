﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using SanPolo.Data;
using SanPolo.Models;
using SanPolo.Services;

namespace SanPolo.Controllers
{
	[Route("api/smwc")]
	[ApiController]
	public class SMWConnectController : SanPoloBaseController<SMWConnectUser, CoreSMWConnectRepository>
	{
		public SMWConnectController(CoreSMWConnectRepository repository, IOptions<HashingOptions> options, IOptions<AppSettings> appSettings, ILogger<CoreSMWConnectRepository> logger) : base(repository, options, appSettings, logger)
		{
		}
	}
}
