﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

using Microsoft.Docs.Samples;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using SanPolo.Models;
using SanPolo.Services;
using SanPolo.Interfaces;

using Swashbuckle.AspNetCore.Annotations;

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace SanPolo.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public abstract class SanPoloBaseController<TEntity, TRepository> : ControllerBase
		where TEntity : class, IEntity
		where TRepository : IRepository<TEntity>
	{
		public readonly TRepository _rep;
		private readonly TokenGenerator tokenGenerator;
		private readonly IOptions<HashingOptions> _options;
		public readonly ILogger _logger;

		public SanPoloBaseController(TRepository repository, IOptions<HashingOptions> options, IOptions<AppSettings> appSettings, ILogger<TRepository> logger)
		{
			_rep = repository;
			_options = options;
			_logger = logger;
			tokenGenerator = new TokenGenerator(appSettings.Value);
		}

		#region CRUD
		/// <summary>
		/// Get all entities
		/// </summary>
		/// <returns>All the entities available</returns>
		[HttpGet]
		[Produces("application/json")]
		[SwaggerResponse(StatusCodes.Status200OK, Description = "Returns all available entites")]
		[SwaggerResponse(StatusCodes.Status400BadRequest, Description = "Returns empty server error message")]
		[SwaggerResponse(StatusCodes.Status401Unauthorized, Description = "If the client is unauthorized")]
		public virtual async Task<ActionResult<IEnumerable<TEntity>>> Get()
		{
			_logger.LogInformation("{routeInfo} - {DateTime.UtcNow}", IsControllerContextEmpty(), DateTime.Now);
			try
			{
				return Ok(await _rep.GetAll());
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Error adding at ${TIME}", DateTime.UtcNow);
				return BadRequest();
			}
		}

		/// <summary>
		/// Get specific entity by id
		/// </summary>
		/// <param name="id"></param>
		/// <returns>Specified entity</returns>
		[HttpGet("{id}")]
		[Produces("application/json")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status401Unauthorized)]
		public virtual async Task<ActionResult<TEntity>> Get(string id)
		{
			_logger.LogInformation("{routeInfo} - {DateTime.UtcNow}", IsControllerContextEmpty(), DateTime.Now);
			try
			{
				var entity = await _rep.Get(id);
				if (entity == null) return NotFound();
				return Ok(entity);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Error adding at ${TIME}", DateTime.UtcNow);
				return BadRequest();
			}
		}

		/// <summary>
		/// Update specific entity by id
		/// </summary>
		/// <param name="id"></param>
		/// <param name="entity"></param>
		/// <returns>The updated entity</returns>
		[HttpPut("{id}")]
		[Produces("application/json")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status401Unauthorized)]
		public virtual async Task<ActionResult<TEntity>> Put(string id, TEntity entity)
		{
			_logger.LogInformation("{routeInfo} - {DateTime.UtcNow}", IsControllerContextEmpty(), DateTime.Now);
			try
			{
				if (Guid.Parse(id) != entity.Id) return NotFound();
				await _rep.Update(entity);
				return Ok(entity);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Error adding at ${TIME}", DateTime.UtcNow);
				return BadRequest();
			}
		}

		/// <summary>
		/// Creates new entity
		/// </summary>
		/// <param name="entity"></param>
		/// <returns>The created entity</returns>
		[HttpPost]
		[Produces("application/json")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status401Unauthorized)]
		public virtual async Task<ActionResult<TEntity>> Post(TEntity entity)
		{
			_logger.LogInformation("{routeInfo} - {DateTime.UtcNow}", IsControllerContextEmpty(), DateTime.Now);
			var pass = new PasswordHasher(_options);
			try
			{
				entity.Password = pass.Hash(entity.Password);
				await _rep.Add(entity);
				return Ok(entity);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Error adding at ${TIME}", DateTime.UtcNow);
				return BadRequest();
			}
		}

		/// <summary>
		/// Deletes specific entity by id
		/// </summary>
		/// <param name="id"></param>
		/// <returns>Success Message</returns>
		[HttpDelete("{id}")]
		[Produces("application/json")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status401Unauthorized)]
		public async Task<ActionResult<TEntity>> Delete(string id)
		{
			_logger.LogInformation("{routeInfo} - {DateTime.UtcNow}", IsControllerContextEmpty(), DateTime.Now);
			try
			{
				var entity = await _rep.Delete(id);
				if (entity == null) return NotFound();
				return Ok($"Deleted entity with id: {id}");
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Error adding at ${TIME}", DateTime.UtcNow);
				return BadRequest();
			}
		}
		#endregion

		#region Auth
		/// <summary>
		/// Main Login Method
		/// </summary>
		/// <param name="payload"></param>
		/// <returns>User Entity with Access Token and HTTP Only Cookie</returns>
		[HttpPost("login")]
		[AllowAnonymous]
		[Produces("application/json")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status401Unauthorized)]
		public async Task<ActionResult<AuthPayload<TEntity>>> Login([FromBody] LoginPayload payload)
		{
			_logger.LogInformation("{routeInfo} - {DateTime.UtcNow}", IsControllerContextEmpty(), DateTime.Now);
			var pass = new PasswordHasher(_options);
			var usr = await _rep.GetByEmail(payload.Email);

			if (usr == null) return NotFound();

			if (pass.Check(usr.Password, payload.Password).Verified)
			{
				try
				{
					var authPayload = new AuthPayload<TEntity>(usr);
					var accessToken = tokenGenerator.GenerateToken(usr);

					var currentRefreshToken = await _rep.GetActiveRefreshTokenByUserId(usr.Id);
					if (currentRefreshToken != null
						&& currentRefreshToken.IsActive 
						&& currentRefreshToken.CreatedByIp == GetIPAddress())
					{
						SetTokenCookie(currentRefreshToken.Token);
					}
					else
					{

						var refreshToken = tokenGenerator.GenerateRefreshToken(GetIPAddress());
						refreshToken.UserId = usr.Id;
						await _rep.AddRefreshToken(refreshToken);
						SetTokenCookie(refreshToken.Token);
					}

					authPayload.AccessToken = accessToken;
					return Ok(authPayload);
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, "Error adding at ${TIME}", DateTime.UtcNow);
					return BadRequest();
				}
			}

			return Unauthorized();
		}

		/// <summary>
		/// Get's you a new Access Refresh Token if authorized
		/// </summary>
		/// <returns>New Access Token</returns>
		[HttpGet("refresh-token")]
		[AllowAnonymous]
		[Produces("application/json")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status401Unauthorized)]
		public async Task<ActionResult> RefreshToken()
		{
			_logger.LogInformation("{routeInfo} - {DateTime.UtcNow}", IsControllerContextEmpty(), DateTime.Now);
			var currentRefreshToken = Request.Cookies["amn"];
			var refreshToken = await _rep.GetActiveRefreshToken(currentRefreshToken, GetIPAddress());

			if (refreshToken == null)
				return Unauthorized();

			try
			{
				var newRefreshToken = tokenGenerator.GenerateRefreshToken(GetIPAddress());
				refreshToken.Revoked = DateTime.Now;
				refreshToken.RevokedByIp = GetIPAddress();
				refreshToken.ReplacedByToken = newRefreshToken.Token;

				newRefreshToken.UserId = refreshToken.UserId;
				await _rep.AddRefreshToken(newRefreshToken);

				SetTokenCookie(newRefreshToken.Token);

				var usr = await _rep.Get(refreshToken.UserId.ToString());

				return Ok(new { token = tokenGenerator.GenerateToken(usr) });
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Error adding at ${TIME}", DateTime.UtcNow);
				return BadRequest();
			}
		}

		/// <summary>
		/// Used when logging out to revoke the last used token by the user
		/// </summary>
		/// <returns></returns>
		[HttpPost("revoke-token")]
		[Produces("application/json")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<ActionResult> RevokeToken()
		{
			_logger.LogInformation("{routeInfo} - {DateTime.UtcNow}", IsControllerContextEmpty(), DateTime.Now);
			try
			{
				var token = Request.Cookies["amn"];

				if (string.IsNullOrEmpty(token))
					return BadRequest(new { message = "Token is required" });

				var response = await _rep.UpdateRefreshToken(token, GetIPAddress());

				if (response == null)
					return NotFound();

				Response.Cookies.Delete("amn");

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Error adding at ${TIME}", DateTime.UtcNow);
				return BadRequest();
			}
		}
		#endregion
		
		/// <summary>
		/// Set's the authentication cookie
		/// </summary>
		/// <param name="token">The refresh token to be set</param>
		private void SetTokenCookie(string token)
		{
			var cookieOptions = new CookieOptions
			{
				HttpOnly = true,
				Expires = DateTime.UtcNow.AddDays(7)
			};
			Response.Cookies.Append("amn", token, cookieOptions);
		}

		/// <summary>
		/// Get's the IP Address of client
		/// </summary>
		/// <returns>The IP Address of the client</returns>
		private string GetIPAddress()
		{
			if (Request.Headers.ContainsKey("X-Forwarded-For"))
				return Request.Headers["X-Forwarded-For"];
			else
				return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
		}

		/// <summary>
		/// Checks if controller context is null, if so return empty string
		/// This extra step is for when tests are run
		/// </summary>
		/// <returns>Controller Info or Empty String</returns>
		private string IsControllerContextEmpty()
		{
			return ControllerContext.RouteData != null ? ControllerContext.ToCtxString() : "";
		}
	}
}
