﻿using Microsoft.EntityFrameworkCore;

using SanPolo.Models;
using SanPolo.Interfaces;

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace SanPolo.Data
{
	public abstract class CoreRepository<TEntity> : IRepository<TEntity>
		where TEntity : class, IEntity
	{
		private readonly SanPoloContext _ctx;

		public CoreRepository(SanPoloContext context)
		{
			_ctx = context;
		}

		#region CRUD
		public virtual async Task<TEntity> Add(TEntity entity)
		{
			entity = AddDates(entity);
			_ctx.Set<TEntity>().Add(entity);
			await _ctx.SaveChangesAsync();
			return entity;
		}

		public virtual async Task<TEntity> Delete(string id)
		{
			var entity = await _ctx.Set<TEntity>().FindAsync(Guid.Parse(id));
			if (entity == null) return entity;

			_ctx.Set<TEntity>().Remove(entity);
			await _ctx.SaveChangesAsync();

			return entity;
		}

		public virtual async Task<TEntity> Get(string id) => await _ctx.Set<TEntity>().FindAsync(Guid.Parse(id));

		public virtual async Task<TEntity> GetByEmail(string email) => await _ctx.Set<TEntity>().Where(usr => usr.Email == email).FirstOrDefaultAsync();

		public virtual async Task<List<TEntity>> GetAll() => await _ctx.Set<TEntity>().ToListAsync();

		public virtual async Task<TEntity> Update(TEntity entity)
		{
			entity.Updated = DateTime.Now;
			_ctx.Set<TEntity>().Attach(entity);
			_ctx.Entry(entity).State = EntityState.Modified;
			await _ctx.SaveChangesAsync();
			return entity;
		}
		#endregion

		#region Auth
		public virtual async Task<RefreshToken> AddRefreshToken(RefreshToken refreshToken)
		{
			if (refreshToken.UserId == null) throw new Exception("User Id wasn't set on the refresh token");
			_ctx.RefreshTokens.Add(refreshToken);
			await _ctx.SaveChangesAsync();
			return refreshToken;
		}

		public virtual async Task<RefreshToken> GetActiveRefreshToken(string token, string ipAddress)
		{
			var allTokens = await _ctx.RefreshTokens.ToListAsync();
			var refreshToken = await _ctx.RefreshTokens.Where(t => t.Token == token).FirstOrDefaultAsync();

			if (refreshToken == null || !refreshToken.IsActive) return null;

			if (refreshToken.CreatedByIp == ipAddress)
				return refreshToken;
			else
				return null;
		}

		public virtual async Task<RefreshToken> GetActiveRefreshTokenByUserId(Guid userId)
			=> await _ctx.RefreshTokens.Where(rf => rf.UserId == userId && rf.Revoked == null).FirstOrDefaultAsync();

		public virtual async Task<RefreshToken> UpdateRefreshToken(string token, string ipAddress)
		{
			var refreshToken = await _ctx.RefreshTokens.Where(rt => rt.Token == token).FirstOrDefaultAsync();

			if (refreshToken != null && refreshToken.CreatedByIp != ipAddress) return null;

			refreshToken.Revoked = DateTime.Now;
			refreshToken.RevokedByIp = ipAddress;

			_ctx.RefreshTokens.Attach(refreshToken);
			_ctx.Entry(refreshToken).State = EntityState.Modified;
			await _ctx.SaveChangesAsync();

			return refreshToken;
		}
		#endregion

		public TEntity AddDates(TEntity entity)
		{
			entity.Created = DateTime.Now;
			entity.Updated = DateTime.Now;
			return entity;
		}
	}
}
