﻿using SanPolo.Models;

namespace SanPolo.Data
{
	public class CoreSMWConnectRepository : CoreRepository<SMWConnectUser>
	{
		public CoreSMWConnectRepository(SanPoloContext context) : base(context)
		{
		}
	}
}
