﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SanPolo.Models
{
	public class RefreshToken
	{
		[Key]
		public Guid Id { get; set; }

		public Guid UserId { get; set; }

		public string Token { get; set; }		

		public string ReplacedByToken { get; set; }

		public string CreatedByIp { get; set; }

		public string RevokedByIp { get; set; }
		
		public bool IsExpired => DateTime.UtcNow >= Expires;

		public bool IsActive => Revoked == null && !IsExpired;
		
		public DateTime? Revoked { get; set; }
		
		public DateTime Created { get; set; }

		public DateTime Expires { get; set; }
	}
}
