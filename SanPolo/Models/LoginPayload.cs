﻿namespace SanPolo.Models
{
	public class LoginPayload
	{
		public string Email { get; set; }

		public string Password { get; set; }
	}
}
