﻿using SanPolo.Interfaces;
using System.Text.Json.Serialization;

namespace SanPolo.Models
{
	public class AuthPayload<TEntity> where TEntity : class, IEntity
	{
		public AuthPayload(TEntity user)
		{
			User = user;
		}

		public TEntity User { get; set; }

		public string AccessToken { get; set; }
	}
}
