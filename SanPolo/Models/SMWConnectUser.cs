﻿using SanPolo.Interfaces;

using System;

namespace SanPolo.Models
{
	public class SMWConnectUser : IEntity
	{
		public string Email { get; set; }

		public string Password { get; set; }

		public Guid Id { get; set; }

		public DateTime Created { get; set; }

		public DateTime Updated { get; set; }
	}
}
