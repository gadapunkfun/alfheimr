﻿using Microsoft.EntityFrameworkCore;

using SanPolo.Models;

namespace SanPolo
{
	public class SanPoloContext : DbContext
	{
		public SanPoloContext(DbContextOptions<SanPoloContext> options) : base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<SMWConnectUser>()
				.Property(prop => prop.Email)
				.IsRequired();
			builder.Entity<SMWConnectUser>()
				.HasIndex(index => index.Email)
				.IsUnique();
		}

		public DbSet<SMWConnectUser> SMWConnectUsers { get; set; }

		public DbSet<RefreshToken> RefreshTokens { get; set; }
	}
}
