﻿using Microsoft.IdentityModel.Tokens;

using SanPolo.Models;
using SanPolo.Interfaces;

using System;
using System.Text;
using System.Security.Claims;
using System.Security.Cryptography;
using System.IdentityModel.Tokens.Jwt;

namespace SanPolo.Services
{
	public class TokenGenerator
	{
		private readonly AppSettings _appSettings;

		public TokenGenerator(AppSettings appSettings)
		{
			_appSettings = appSettings;
		}

		public string GenerateToken(IEntity entity)
		{
			var tokenHandler = new JwtSecurityTokenHandler();
			var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
			var tokenDescriptor = new SecurityTokenDescriptor
			{
				Subject = new ClaimsIdentity(new[]
				{
					new Claim(ClaimTypes.Name,
					entity.Id.ToString())
				}),
				Expires = DateTime.UtcNow.AddDays(7),
				SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
			};

			var token = tokenHandler.CreateToken(tokenDescriptor);
			return tokenHandler.WriteToken(token);
		}

		public RefreshToken GenerateRefreshToken(string ipAddress)
		{
			using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
			{
				var randomBytes = new byte[64];
				rngCryptoServiceProvider.GetBytes(randomBytes);
				return new RefreshToken
				{
					Token = Convert.ToBase64String(randomBytes),
					Expires = DateTime.UtcNow.AddDays(7),
					Created = DateTime.UtcNow,
					CreatedByIp = ipAddress
				};
			}
		}
	}
}
