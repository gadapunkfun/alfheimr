﻿namespace SanPolo.Services
{
	public sealed class HashingOptions
	{
		public int Iterations { get; set; } = 10000;
	}
}
