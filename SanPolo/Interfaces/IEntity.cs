﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SanPolo.Interfaces
{
	public interface IEntity
	{
		[Key]
		public Guid Id { get; set; }

		public string Email { get; set; }

		public string Password { get; set; }

		public DateTime Created { get; set; }

		public DateTime Updated { get; set; }
	}
}
