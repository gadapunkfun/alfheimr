﻿using SanPolo.Models;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SanPolo.Interfaces
{
	public interface IRepository<TEntity> where TEntity : class, IEntity
	{
		public Task<List<TEntity>> GetAll();

		public Task<TEntity> Get(string id);

		public Task<TEntity> GetByEmail(string email);

		public Task<RefreshToken> GetActiveRefreshToken(string token, string ipAddress);

		public Task<RefreshToken> GetActiveRefreshTokenByUserId(Guid userId);

		public Task<TEntity> Add(TEntity entity);

		public Task<RefreshToken> AddRefreshToken(RefreshToken refreshToken);

		public Task<TEntity> Update(TEntity entity);

		public Task<RefreshToken> UpdateRefreshToken(string token, string ipAddress);

		public Task<TEntity> Delete(string id);
	}
}
