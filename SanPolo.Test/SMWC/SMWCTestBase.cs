﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

using System;

using SanPolo.Models;
using SanPolo.Services;

namespace SanPolo.Test.SMWC
{
	public class SMWCTestBase : IDisposable
	{
		protected readonly SanPoloContext _ctx;

		protected readonly IOptions<HashingOptions> _options;

		protected readonly IOptions<AppSettings> _appSettings;

		public SMWCTestBase()
		{
			var ctx = new DbContextOptionsBuilder<SanPoloContext>()
				.UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
				.Options;

			_options = Options.Create(new HashingOptions());
			_appSettings = Options.Create(new AppSettings());

			_ctx = new SanPoloContext(ctx);
			_ctx.Database.EnsureCreated();
		}

		public void Dispose()
		{
			_ctx.Database.EnsureDeleted();
			_ctx.Dispose();
		}
	}
}
