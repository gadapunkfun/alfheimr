﻿using FluentAssertions;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using SanPolo.Data;
using SanPolo.Models;
using SanPolo.Controllers;

using System;

using Xunit;
using System.Collections.Generic;

namespace SanPolo.Test.SMWC.Unit
{
	public class SMWConnectControllerTests : SMWCTestBase
	{
		public Guid userId;

		public SMWConnectUser user;

		public SMWConnectControllerTests()
		{
			userId = Guid.NewGuid();
			user = new SMWConnectUser
			{
				Id = userId,
				Email = "test@test.com",
				Password = "10000.GKerjeGjiiE9kOguu5fP+g==.seokVkX2BYCMp7nSU7fG3oEQgyYecydyVgsVqo3oJCA=",
				Created = DateTime.Now,
				Updated = DateTime.Now
			};

			_ctx.SMWConnectUsers.Add(user);
			_ctx.SaveChanges();
		}

		[Fact]
		public async void GetAll()
		{
			// Arrange
			var logger = new LoggerFactory().CreateLogger<CoreSMWConnectRepository>();
			var controller = new SMWConnectController(new CoreSMWConnectRepository(_ctx), _options, _appSettings, logger);

			// Act
			var result = await controller.Get();
			var formattedResult = result.Result?.As<OkObjectResult>().Value.As<List<SMWConnectUser>>();

			// Assert
			formattedResult.Should().NotBeNull();
			formattedResult.Should().BeOfType<List<SMWConnectUser>>();
		}

		[Fact]
		public async void Get()
		{
			// Arrange
			var logger = new LoggerFactory().CreateLogger<CoreSMWConnectRepository>();
			var controller = new SMWConnectController(new CoreSMWConnectRepository(_ctx), _options, _appSettings, logger);

			// Act
			var result = await controller.Get(userId.ToString());
			var formattedResult = result.Result?.As<OkObjectResult>().Value.As<SMWConnectUser>();

			// Assert
			formattedResult.Should().NotBeNull();
			formattedResult.Should().BeOfType<SMWConnectUser>();
			formattedResult.Email.Should().Be("test@test.com");
		}

		[Fact]
		public async void Put()
		{
			// Arrange
			var logger = new LoggerFactory().CreateLogger<CoreSMWConnectRepository>();
			var controller = new SMWConnectController(new CoreSMWConnectRepository(_ctx), _options, _appSettings, logger);

			// Act
			user.Email = "test@test.xyz";
			var result = await controller.Put(userId.ToString(), user);
			var formattedResult = result.Result?.As<OkObjectResult>().Value.As<SMWConnectUser>();

			// Assert
			formattedResult.Should().NotBeNull();
			formattedResult.Should().BeOfType<SMWConnectUser>();
			formattedResult.Email.Should().Be("test@test.xyz");
			formattedResult.Email.Should().NotBe("test@test.com");
		}

		[Fact]
		public async void Post()
		{
			// Arrange
			var logger = new LoggerFactory().CreateLogger<CoreSMWConnectRepository>();
			var controller = new SMWConnectController(new CoreSMWConnectRepository(_ctx), _options, _appSettings, logger);

			// Act
			var result = await controller.Post(new SMWConnectUser
			{
				Email = "sample@sample.com",
				Password = "huemaster"
			});
			var formattedResult = result.Result?.As<OkObjectResult>().Value.As<SMWConnectUser>();

			// Assert
			formattedResult.Should().NotBeNull();
			formattedResult.Should().BeOfType<SMWConnectUser>();
			formattedResult.Email.Should().Be("sample@sample.com");
		}

		[Fact]
		public async void Delete()
		{
			// Arrange
			var logger = new LoggerFactory().CreateLogger<CoreSMWConnectRepository>();
			var controller = new SMWConnectController(new CoreSMWConnectRepository(_ctx), _options, _appSettings, logger);

			// Act
			var result = await controller.Delete(userId.ToString());

			// Assert
			result.Result.Should().BeOfType<OkObjectResult>();
		}
	}
}
